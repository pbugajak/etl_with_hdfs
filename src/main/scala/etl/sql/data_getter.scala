package etl.sql

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.rand
import org.apache.spark.sql.types.IntegerType


object data_getter {
  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder()
    .appName("ETL with SQL")
    .master("local[2]")
    .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")

    val salesJan: RDD[String] = spark.sparkContext.textFile("data/SalesJan2009.csv")

    val dataFrame = spark.read
    .option("header", "true")
    .csv("data/SalesJan2009.csv")

    //    val header: String = salesJan.first()
    //
    //    val salesWithoutHeader: RDD[String] = salesJan.filter(criminalRecord => criminalRecord != header)
    //    val df = salesWithoutHeader.toDF()
    //
    //    val singleRecord: String = salesWithoutHeader.first()
    //    println(singleRecord)



    val outputPath = new java.io.File("data/output").getCanonicalPath
    //    dataFrame
    //      .repartition(col("Payment_Type"))
    //      .write
    //      .partitionBy("Payment_Type")
    //      .parquet(outputPath)

    //    dataFrame
    //      .repartition(5)
    //      .write
    //      .partitionBy("Payment_Type")
    //      .parquet(outputPath)
//
//    dataFrame
//      .repartition(3, col("Payment_Type"), rand)
//      .write
//      .partitionBy("Payment_Type")
//      .csv(outputPath)

    val countDF = dataFrame.groupBy("Payment_Type").count()
    val desiredRowsPerPartition = 10
    val joinedDF = dataFrame
      .join(countDF, Seq("Payment_Type"))
      .withColumn(
        "my_secret_partition_key",
        (rand(10) * col("count") / desiredRowsPerPartition).cast(IntegerType)
      )
    joinedDF
      .repartition(col("Payment_Type"), col("my_secret_partition_key"))
      .drop("count", "my_secret_partition_key")
      .write
      .partitionBy("Payment_Type")
      .csv(outputPath)



//    val timeWithPaymentType: (Int, String, Int) = {
//      val singleValueArray: Array[String] = singleRecord.split(",")
//      val date: String = singleValueArray(0).split(" ")(0)
//      val expectedDate: String = date.split("/")(2)+date.split("/")(1)
//      (expectedDate.toInt, singleValueArray(3), 1)
//    }
//
//    val salesRecordWithMonthAndType = salesWithoutHeader
//      .map(singleRecord => {
//        val singleValueArray: Array[String] = singleRecord.split(",")
//        val date: String = singleValueArray(0).split(" ")(0)
//        val expectedDate: String = date.split("/")(2)+date.split("/")(1)
//        ((expectedDate.toInt, singleValueArray(3)), 1)
//      })
//
//    val salesCountPerMonthPerType: RDD[((Int, String), Int)] = salesRecordWithMonthAndType.reduceByKey((total, value) => total + value)
//
//    val expectedMapSalesCountPerMonthPerTypeSorted = salesCountPerMonthPerType
//      .map(singleRecord => ((singleRecord._1._1, -singleRecord._2), singleRecord._1._1 + "\t" + singleRecord._2 + "\t" + singleRecord._1._2))
//      .sortByKey()
//      .map(singleRecord => singleRecord._2)
//
//    println(expectedMapSalesCountPerMonthPerTypeSorted.take(880).foreach(println))

    spark.stop()
  }

}
